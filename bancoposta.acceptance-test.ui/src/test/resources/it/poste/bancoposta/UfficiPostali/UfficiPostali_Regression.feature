@UfficiPostali
Feature: Uffici Postali
  L utente cerca un ufficio postale intorno a lui

  @BPINPROAND-1570 @BPINPROIOS-4218
  Scenario Outline: Cerca Ufficio Postale
      Viene cercato un Ufficio Postale nella relativa sezione

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    And Naviga verso la sezione cerca Ufficio Postale
    Then Controlla la presenza di un ufficio postale nelle vicinanze
    And Viene chiusa l'app

    @BPINPROAND-1570_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-1570 | nonseitu   |

    @BPINPROAND-1570_vol
    Examples: 
      | username  | password   | loginType | userHeader | device               | posteID | testIdCheck     | onboarding |
      | fedro.vol | Password1! | poste.it  | Fedro      | samsungGalaxyS8_plus | 123123  | BPINPROAND-1570 | nonseitu   |

    @BPINPROAND-1570_romani
    Examples: 
      | username          | password   | loginType | userHeader   | device               | posteID | testIdCheck     | onboarding |
      | prisma20@poste.it | Password2! | poste.it  | Maria Grazia | samsungGalaxyS8_plus | 123123  | BPINPROAND-1570 | nonseitu   |

   	@BPINPROIOS-4218_mancini
    Examples: 
      | username          | password   | loginType | userHeader   | device               | testIdCheck     | posteID | onboarding |
      | prisma21@poste.it | Password1! | poste.it  | Antonio      | samsungGalaxyS8_plus | BPINPROIOS-4218 | 123123  | nonseitu   |
      
  @PBPAPP-????
  Scenario Outline: Cerca Ufficio Postale test negativo
      Viene cercato un Ufficio Postale nella relativa sezione e viene restituito un risultato negativo

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    And Naviga verso la sezione cerca Ufficio Postale versione negativa
    Then Controlla la presenza di un ufficio postale nelle vicinanze versione negativa
    And Viene chiusa l'app

    @Test-PBPAPP-????
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-1570 | nonseitu   |
