@Salvadanaio
Feature: Salvadanaio
  L'utente interagisce con il salvadanaio

  @BPINPROAND-1249
  Scenario Outline: Salvadanaio - Automatismo - verifiche formali sui campi
     Salvadanaio - Automatismo - verifiche formali sui campi

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> |
    And Naviga verso il salvadanaio
    And Seleziona un obiettivo
      | objectiveName   |
      | <objectiveName> |
    And Naviga verso la sezione per effettuare un versamento
    And seleziona il versamento sul salvadanaio
      | versamentoType   | payWith   |
      | <versamentoType> | <payWith> |
    And Effettua i controlli sui campi
      | versamentoType   | payWith   |
      | <versamentoType> | <payWith> |
    And Viene chiusa l'app

    @BPINPROAND-1249_musella
    Examples: 
      | versamentoType | username             | password  | loginType | userHeader | device               | payWith            | transferTo       | owner              | amount | month | categoryDestination | posteID | description                         | testIdCheck     | messageText                                | transactionType | objectiveName     | onboarding |
      | random         | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077144679 | VINCENZO FORTUNATO |   0.01 | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1249 | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP | controlli formali | nonseitu   |

  @BPINPROAND-1133
  Scenario Outline: Verifica Limite Salvadanaio
    verificare il limite massimo del salvadanaio

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1133 | <onboarding> |
    And Naviga verso il salvadanaio
    And Crea un obiettivo
      | data   | amount   | nomeObiettivo   | importo2   | iban   |
      | <data> | <amount> | <nomeObiettivo> | <importo2> | <iban> |
    Then il popup di errore "Importo Massimo Raggiunto" viene mostrato
    And Viene chiusa l'app

    @BPINPROAND-1133_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | amount | importo2 | iban         | data       | nomeObiettivo | posteID | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | 500000 | 5.000,00 | 000049494216 | 13/05/2020 | prova         | prisma  | PBPAPP-3158 | nonseitu   |

  @BPINPROAND-1264
  Scenario Outline: Salvadanaio H24 - Crea e Modifica obiettivo
     Salvadanaio H24 - Crea e Modifica obiettivo

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1264 | <onboarding> |
    And Naviga verso la sezione conti e carte
    And Check importo disponibile
      | paymentMethod   | amount2   | payWith   |
      | <paymentMethod> | <amount2> | <payWith> |
    And Naviga verso il salvadanaio
    And Viene cliccato il pulsante Crea nuovo obiettivo
      | objectiveName   | posteid   | evolutionNumber   | owner   |
      | <objectiveName> | <posteID> | <evolutionNumber> | <owner> |
    And Viene selezionato l obiettivo
    And Viene selezionata la durata e la data dell obiettivo
    And Viene inserito l importo
    And Viene controllato il riepilogo e inserito il nome obiettivo
      | objectiveName   |
      | <objectiveName> |
    And Si chiude la thank you page cliccando sul pulsante non ora
    And controllo obiettivo creato
      | objectiveName   |
      | <objectiveName> |
    And Seleziona un obiettivo
      | objectiveName   |
      | <objectiveName> |
    And Modifica l obiettivo creato
      | objectiveNameNew   | category   | importo   | importo2   |
      | <objectiveNameNew> | <category> | <importo> | <importo2> |
    And controllo obiettivo creato due
      | objectiveNameNew   | importo2   |
      | <objectiveNameNew> | <importo2> |
    And viene chiuso l obiettivo
      | objectiveNameNew   | posteid   | evolutionNumber   | owner   |
      | <objectiveNameNew> | <posteID> | <evolutionNumber> | <owner> |
    And Viene chiusa l'app

    @BPINPROAND-1264_musella
    Examples: 
      | username             | password  | loginType | userHeader | payWith            | device               | owner          | amount | messageText                                | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description                         | testIdCheck | messageText                                | transactionType | objectiveName | objectiveNameNew | category | importo | importo2 | evolutionNumber | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | conto;001044548509 | samsungGalaxyS8_plus | Salvatore REGA |   0.01 | BancoPosta: Ricarica Carta Postepay da App | Ricarica Postep    |      0.02 | varie              | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1264 | BancoPosta: Ricarica Carta Postepay da App | RICARICA POSTEP | dinamico      | dinamico         | Altro    |   55,00 |   120,20 | **** 7177       | nonseitu   |

  @BPINPROAND-1864
  Scenario Outline: Accantonamento sull'obiettivo salvando come operazione veloce
    Utente actual con un obiettivo (con requisiti minimi per attivare il servizio cioè con libretto Smart e conto e con postepay Evolution onbordati), 
    eseguire un accantonamento partendo dalla schermata Versa sull'obiettivo, l'inizio del funnel accantonamento. Successivamente l'accantonamento come operazione veloce.

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1864 | <onboarding> |
    And Naviga verso il salvadanaio
    And Seleziona un obiettivo
      | objectiveName   |
      | <objectiveName> |
    And Naviga verso la sezione per effettuare un versamento
    And Effettua un accantonamento sull'obiettivo
      | payWith   | amount   | owner   | posteid   |
      | <payWith> | <amount> | <owner> | <posteID> |
    And Salva l accantonamento come operazione veloce
      | quickOperationName   | posteid   |
      | <quickOperationName> | <posteID> |
    And Naviga verso la sezione operazioni veloci
      | quickOperationName2   | userHeader   | payWith   | objectiveName   | amount   |
      | <quickOperationName2> | <userHeader> | <payWith> | <objectiveName> | <amount> |
    And Controlla che il versamento sia presente nella lista dei versamenti
      | messageText   | objectiveName   | amount   |
      | <messageText> | <objectiveName> | <amount> |
    And Viene chiusa l'app

    @BPINPROAND-1864_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo       | owner          | amount | month | categoryDestination | posteID | description                         | testIdCheck | transactionType | objectiveName | quickOperationName | quickOperationName2 | messageText                             | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077144679 | Salvatore Rega |   0.01 | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1864 | RICARICA POSTEP | non toccare   | accantonamento     | salvadanaio         | Versamento dalla tua Postepay Evolution | nonseitu   |

  @BPINPROAND-1186
  Scenario Outline: Salvadanaio - Accantonamento ricorrente
    Utente actual con un obiettivo (con requisiti minimi per attivare il servizio cioè con libretto Smart e conto e con postepay Evolution onbordati), 
    eseguire un accantonamento partendo dalla schermata Versa sull'obiettivo, l'inizio del funnel accantonamento. Successivamente l'accantonamento come operazione veloce.

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1186 | <onboarding> |
    And Naviga verso il salvadanaio
    And Seleziona un obiettivo
      | objectiveName   |
      | <objectiveName> |
    And Naviga verso la sezione dei versamenti ricorrenti ed elimina l accantonamento se presente
      | payWith   | amount   | owner   | posteid   | objectiveName   | frequency   | startDate   | endDate   |
      | <payWith> | <amount> | <owner> | <posteID> | <objectiveName> | <frequency> | <startDate> | <endDate> |
    And Naviga verso la sezione per effettuare un versamento
    And Effettua un accantonamento ricorrente sull'obiettivo
      | payWith   | amount   | owner   | posteid   | objectiveName   | frequency   | startDate   | endDate   |
      | <payWith> | <amount> | <owner> | <posteID> | <objectiveName> | <frequency> | <startDate> | <endDate> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1186_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo       | owner             | amount | month | categoryDestination | posteID | description                         | testIdCheck | transactionType       | objectiveName | quickOperationName | quickOperationName2 | messageText                                   | frequency   | startDate | endDate           | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077144679 | Salvatore Musella |   0.01 | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1186 | versamento ricorrente | non toccare   | accantonamento     | salvadanaio         | Conferma versamento ricorrente su Salvadanaio | Ogni giorno | Oggi      | Al raggiungimento | nonseitu   |

    @BPINPROAND-1186_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device        | payWith            | transferTo       | owner              | amount | month | categoryDestination | posteID | description                         | testIdCheck | transactionType       | objectiveName | quickOperationName | quickOperationName2 | messageText                                   | frequency   | startDate | endDate           | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | GalaxyS10Luca | conto;001044920229 | 5333171077144679 | Vincenzo Fortunato |   0.01 | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1186 | versamento ricorrente | non toccare   | accantonamento     | salvadanaio         | Conferma versamento ricorrente su Salvadanaio | Ogni giorno | Oggi      | Al raggiungimento | nonseitu   |

  @BPINPROAND-1265
  Scenario Outline: Salvadanaio - Modifica Accantonamento ricorrente
    Come utente actual del Salvadanaio, voglio modificare una regola di accantonamento automatico esistente

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1265 | <onboarding> |
    And Naviga verso il salvadanaio
    And Seleziona un obiettivo
      | objectiveName   |
      | <objectiveName> |
    And Naviga verso la sezione dei versamenti ricorrenti
      | payWith   | amount   | owner   | posteid   | objectiveName   | frequency   | startDate   | endDate   |
      | <payWith> | <amount> | <owner> | <posteID> | <objectiveName> | <frequency> | <startDate> | <endDate> |
    And Modifica il versamento ricorrente
      | amount   | amount2   | payWith   | modifiedFrequency   | owner   | startDate   | endDate   | posteid   |
      | <amount> | <amount2> | <payWith> | <modifiedFrequency> | <owner> | <startDate> | <endDate> | <posteID> |
    Then Viene controllata la ricezione della notifica
      | messageText   | testIdCheck | notifyType        |
      | <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1265_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo       | owner             | amount | amount2 | month | categoryDestination | posteID | description                         | testIdCheck | transactionType       | objectiveName | quickOperationName | quickOperationName2 | messageText                                   | frequency   | startDate | endDate           | modifiedFrequency | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077144679 | Salvatore Musella |   0.10 |    0.02 | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1265 | versamento ricorrente | mod obj       | accantonamento     | salvadanaio         | Conferma versamento ricorrente su Salvadanaio | Ogni giorno | Oggi      | Al raggiungimento | Ogni 7 giorni     | nonseitu   |

  @BPINPROAND-1217
  Scenario Outline: Salvadanaio - Elimina Accantonamento ricorrente
    Come utente actual del Salvadanaio, voglio eliminare una regola di accantonamento automatico esistente

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1217 | <onboarding> |
    And Naviga verso il salvadanaio
    And Seleziona un obiettivo
      | objectiveName   |
      | <objectiveName> |
    And Naviga verso la sezione dei versamenti ricorrenti
      | payWith   | amount   | owner   | posteid   | objectiveName   | frequency   | startDate   | endDate   |
      | <payWith> | <amount> | <owner> | <posteID> | <objectiveName> | <frequency> | <startDate> | <endDate> |
    And Elimina il versamento ricorrente
      | posteid   |
      | <posteID> |
    #Then Viene controllata la ricezione della notifica
    #| messageText   | testIdCheck | notifyType        |
    #| <messageText> | -           | <transactionType> |
    And Viene chiusa l'app

    @BPINPROAND-1217_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo       | owner          | amount | amount2 | month | categoryDestination | posteID | description                         | testIdCheck | transactionType       | objectiveName | quickOperationName | quickOperationName2 | messageText                                         | frequency   | startDate | endDate           | modifiedFrequency | amount3 | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077144679 | Salvatore Rega |   0.10 |    0.02 | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1217 | versamento ricorrente | non toccare   | accantonamento     | salvadanaio         | Disattivazione versamento ricorrente su Salvadanaio | Ogni giorno | Oggi      | Al raggiungimento | Ogni 7 giorni     |  100.00 | nonseitu   |

    @BPINPROAND-1217_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device        | payWith            | transferTo       | owner          | amount | amount2 | month | categoryDestination | posteID | description                         | testIdCheck | transactionType       | objectiveName | quickOperationName | quickOperationName2 | messageText                                         | frequency   | startDate | endDate           | modifiedFrequency | amount3 | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | conto;001044920229 | 5333171077144679 | Salvatore Rega |   0.10 |    0.02 | month | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1217 | versamento ricorrente | non toccare   | accantonamento     | salvadanaio         | Disattivazione versamento ricorrente su Salvadanaio | Ogni giorno | Oggi      | Al raggiungimento | Ogni 7 giorni     |  100.00 | nonseitu   |

  @BPINPROAND-1867
  Scenario Outline: Chiudere un obiettivo parzialmente raggiunto
    Chiudere un obiettivo parzialmente raggiunto nella fascia oraria 8:00-22:00

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1867 | <onboarding> |
    And Naviga verso il salvadanaio
    And Viene cliccato il pulsante Crea nuovo obiettivo
      | objectiveName   | posteid   | evolutionNumber   | owner   |
      | <objectiveName> | <posteID> | <evolutionNumber> | <owner> |
    And Viene inserito l importo
    And Viene selezionata la durata e la data dell obiettivo
    And Viene selezionato l obiettivo
    And Viene controllato il riepilogo e inserito il nome obiettivo
      | objectiveName   |
      | <objectiveName> |
    And Si chiude la thank you page cliccando sul pulsante non ora
    And controllo obiettivo creato
      | objectiveName   |
      | <objectiveName> |
    And Seleziona un obiettivo
      | objectiveName   |
      | <objectiveName> |
    And Naviga verso la sezione per effettuare un versamento
    And Effettua un accantonamento sull'obiettivo due
      | payWith   | amount   | owner   | posteid   | versamentoType |
      | <payWith> | <amount> | <owner> | <posteID> | singolo        |
    And controllo obiettivo creato
      | objectiveName   |
      | <objectiveName> |
    And viene chiuso l obiettivo2
      | objectiveName   | posteid   | evolutionNumber   | owner   |
      | <objectiveName> | <posteID> | <evolutionNumber> | <owner> |
    And Viene chiusa l'app

    @BPINPROAND-1867_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | transferTo       | owner             | amount | amount2 | categoryDestination | posteID | description                         | testIdCheck | transactionType | objectiveName | quickOperationName | quickOperationName2 | messageText                             | header           | cancel  | CloseObjDescription                                    | evolutionNumber | paymentMethod | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077144679 | Salvatore Musella |   0.01 |    0,01 | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1867 | RICARICA POSTEP | dinamico      | accantonamento     | salvadanaio         | Versamento dalla tua Postepay Evolution | Chiudi obiettivo | ANNULLA | Seleziona lo strumento su cui effettuare il versamento |    001044548509 | conto         | nonseitu   |

    @BPINPROAND-1867_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device        | payWith            | transferTo       | owner              | amount | amount2 | categoryDestination | posteID | description                         | testIdCheck | transactionType | objectiveName | quickOperationName | quickOperationName2 | messageText                             | header           | cancel  | CloseObjDescription                                    | evolutionNumber | paymentMethod | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | GalaxyS10Luca | conto;001044920229 | 5333171077144679 | Vincenzo Fortunato |   0.01 |    0,01 | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1867| RICARICA POSTEP | dinamico      | accantonamento     | salvadanaio         | Versamento dalla tua Postepay Evolution | Chiudi obiettivo | ANNULLA | Seleziona lo strumento su cui effettuare il versamento |    001044920229 | conto         | nonseitu   |

  @BPINPROAND-1865
  Scenario Outline: Creare un nuovo obiettivo (no categoria altro, no calendario)
    Creare un nuovo obiettivo partendo dalla schermata Salvadanaio (no categoria altro, no calendario)

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1865 | <onboarding> |
    And Naviga verso la sezione conti e carte
    And Check importo disponibile
      | paymentMethod   | amount2   | payWith   |
      | <paymentMethod> | <amount2> | <payWith> |
    And Naviga verso il salvadanaio
    And Viene cliccato il pulsante Crea nuovo obiettivo
      | objectiveName   | posteid   | evolutionNumber   | owner   |
      | <objectiveName> | <posteID> | <evolutionNumber> | <owner> |
    And Viene selezionato l obiettivo
    And Viene selezionata la durata e la data dell obiettivo
    And Viene inserito l importo
    And Viene controllato il riepilogo e inserito il nome obiettivo
      | objectiveName   |
      | <objectiveName> |
    And Si chiude la thank you page cliccando sul pulsante non ora
    And controllo obiettivo creato
      | objectiveName   |
      | <objectiveName> |
    And viene chiuso l obiettivo2
      | objectiveName   | posteid   | evolutionNumber   | owner   |
      | <objectiveName> | <posteID> | <evolutionNumber> | <owner> |
    And Viene chiusa l'app

    @BPINPROAND-1865_musella
    Examples: 
      | username               | password  | loginType | userHeader | device               | payWith            | transferTo       | owner             | amount | amount2 | categoryDestination | posteID | description                         | testIdCheck | transactionType | objectiveName    | quickOperationName | quickOperationName2 | messageText                             | header           | cancel  | CloseObjDescription                                    | evolutionNumber | paymentMethod | onboarding |
      | salvatore.musella-1959 | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 | 5333171077144679 | Salvatore Musella |   0.01 |    0,01 | categoryDestination | prisma  | Verifica i dati prima di procedere. | BPINPROAND-1865 | RICARICA POSTEP | Obiettivo Creato | accantonamento     | salvadanaio         | Versamento dalla tua Postepay Evolution | Chiudi obiettivo | ANNULLA | Seleziona lo strumento su cui effettuare il versamento | **** 7177       | evolution     | nonseitu   |
