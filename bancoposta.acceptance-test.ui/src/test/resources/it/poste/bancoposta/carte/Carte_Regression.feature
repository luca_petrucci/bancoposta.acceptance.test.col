@Carte
Feature: Carte
  L'utente dopo aver onboardato il conto ed essersi posizionato sulla carta stessa visualizza l'elenco dei movimenti presenti

  @BPINPROAND-1578
  Scenario Outline: Lista Movimenti Carta
    L'utente visualizza la lista dei movimenti della cart

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1578 | <onboarding> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti due
      | conto     | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <transactionType> | <transactionDescription> | <commissioni> |
    And Viene cliccato il primo risultato disponibile
    And L utente verifica il dettaglio del movimento
    Then Viene chiusa l'app

    @BPINPROAND-1578_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | transactionType | transactionDescription |  posteID | commissioni |  testIdCheck     | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | BONIFICO        | TRN                    |  prisma  |         1.0 |  BPINPROAND-1578 | nonseitu   |

    @BPINPROAND-1578_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith                   | transactionType | transactionDescription | posteID | commissioni | testIdCheck     | onboarding |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | postepay;5333171077144679 | BONIFICO        | TRN                    | prisma  |         1.0 | BPINPROAND-1578 | nonseitu   |

  @BPINPROAND-1315
  Scenario Outline: Lista Verificare gli elementi presenti nella schermata di dettaglio di una Postepay
    Verificare gli elementi presenti nella schermata di dettaglio di una Postepay

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   | debug |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1315 | <onboarding> | y     |
    And Naviga verso la sezione postepay
      | payWith   | transferTo   | owner   | amount   | testIdCheck     | description   | posteid   | paymentMethod   |
      | <payWith> | <transferTo> | <owner> | <amount> | BPINPROAND-1315 | <description> | <posteID> | <paymentMethod> |
    Then viene controllata la pagina postepay
      | payWith   | transferTo   | owner   | amount   | testIdCheck     | description   | posteid   | paymentMethod   |
      | <payWith> | <transferTo> | <owner> | <amount> | BPINPROAND-1315 | <description> | <posteID> | <paymentMethod> |
    And Viene chiusa l'app

    @BPINPROAND-1315_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | iban         | city   | owner         | amount | description | address                     | citySender | reference | refecenceEffective | quickOperationName | transactionType | transactionDescription | messageText                                   | categoryTransation | editValue | posteID | commissioni | bonificoType | beneficiario  | paymentMethod | testIdCheck | onboarding |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 | 001046919773 | Italia | PETRUCCI LUCA |   0.01 | login       | Via degli Ulivi Gialli n.23 | Agrigento  | Luca      | Luca Petrucci      | girofondo          | BONIFICO        | TRN                    | BancoPosta: Conferma Bonifico SEPA effettuato | Varie              |       0.2 | prisma  |         1.0 | bonifico     | PETRUCCI LUCA | carta         | BPINPROAND-1315 | nonseitu   |
