@RicaricaTelefonica
Feature: Ricarica Telefonica
  L'utente compila e sottomette una ricarica Telefonica, da sottomenu paga o da operazione veloce

  @BPINPROAND-1547 @BPINPROIOS-1397
  Scenario Outline: Ricarica Telefonica da conto
     Viene effettuata una ricarica telefonica da conto

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1547 | <onboarding> |
    And Viene effettuata una "ricarica telefonica" da "conti e carte" section
      | payWith   | amount   | testIdCheck | posteid   | phoneNumber   | phoneCompaign   |
      | <payWith> | <amount> | -           | <posteID> | <phoneNumber> | <phoneCompaign> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   |
      | <quickOperationName> |
    And Viene controllata la ricezione della notifica
      | messageText   |
      | <messageText> |
    And Viene effettuata la transazione da operazione veloce
      | quickOperationName   | testIdCheck |
      | <quickOperationName> | -           |
    And Viene controllata la ricezione della notifica
      | messageText   | testIdCheck |
      | <messageText> | -           |
    And Viene controllata la corretta categorizzazione della transazione
      | categoryTransation   | amount   | testIdCheck |
      | <categoryTransation> | <amount> | -           |
    Then Viene controllata la ricezione della notifica
      | messageText   | notifyType   |
      | <messageText> | <notifyType> |
    And Viene chiusa l'app

    @BPINPROAND-1547_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText         | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description         | onboarding | notifyType          | messageText                             |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 |   356874982 | PosteMobile   |   5,00 | ricarica telefonica | quickOperationName | editValue | categoryTransation | month | categoryDestination | prisma  | ricarica telefonica | nonseitu   | ricarica telefonica | Conferma Ricarica telefonica effettuata |

    @BPINPROAND-1547_vol
    Examples: 
      | username  | password   | loginType | userHeader | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText         | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description         | onboarding | notifyType          | messageText                             |
      | fedro.vol | Password1! | poste.it  | Fedro      | samsungGalaxyS8_plus | conto;001009125285 |   356874982 | PosteMobile   |   5,00 | ricarica telefonica | RicaricaTel        | editValue | categoryTransation | month | categoryDestination |  123123 | ricarica telefonica | nonseitu   | ricarica telefonica | Conferma Ricarica telefonica effettuata |

    @BPINPROAND-1547_romani
    Examples: 
      | username          | password   | loginType | userHeader   | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText         | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description         | onboarding | notifyType          | messageText                             |
      | prisma20@poste.it | Password2! | poste.it  | Maria Grazia | samsungGalaxyS8_plus | conto;001009187830 |   356874982 | PosteMobile   |   5,00 | ricarica telefonica | RicaricaTel        | editValue | categoryTransation | month | categoryDestination |  123123 | ricarica telefonica | nonseitu   | ricarica telefonica | Conferma Ricarica telefonica effettuata |

		@BPINPROAND-1547_marinello
    Examples: 
      | username          | password   | loginType | userHeader   | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText         | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description         | onboarding | notifyType          | messageText                             |
      | prisma65@poste.it | Password2! | poste.it  | Fiorella     | samsungGalaxyS8_plus | conto;001009197300 |   356874982 | PosteMobile   |   5,00 | ricarica telefonica | RicaricaTel        | editValue | categoryTransation | month | categoryDestination |  123123 | ricarica telefonica | nonseitu   | ricarica telefonica | Conferma Ricarica telefonica effettuata |
      
    @BPINPROIOS-1397_mancini
    Examples: 
      | username          | password   | loginType | userHeader | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText         | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description         | onboarding | notifyType          | messageText                             |
      | prisma21@poste.it | Password1! | poste.it  | Antonio    | samsungGalaxyS8_plus | conto;001009187798 |   356874982 | PosteMobile   |   5,00 | ricarica telefonica | RicaricaTel        | editValue | categoryTransation | month | categoryDestination |  123123 | ricarica telefonica | nonseitu   | ricarica telefonica | Conferma Ricarica telefonica effettuata |

		@BPINPROIOS-1397_fanucci
    Examples: 
      | username          | password   | loginType | userHeader  | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText         | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description         | onboarding | notifyType          | messageText                             |
      | prisma33@poste.it | Password1! | poste.it  | Vincenza    | samsungGalaxyS8_plus | conto;001009200237 |   356874982 | PosteMobile   |   5,00 | ricarica telefonica | RicaricaTel        | editValue | categoryTransation | month | categoryDestination |  123123 | ricarica telefonica | nonseitu   | ricarica telefonica | Conferma Ricarica telefonica effettuata |
  
  @BPINPROAND-1547_fiorella
    Examples: 
      | username          | password   | loginType | userHeader  | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText         | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description         | onboarding | notifyType          | messageText                             |
      | prisma65@poste.it | Password1! | poste.it  | Fiorella    | samsungGalaxyS8_plus | conto;001009197300 |   356874982 | PosteMobile   |   5,00 | ricarica telefonica | RicaricaTel        | editValue | categoryTransation | month | categoryDestination |  123123 | ricarica telefonica | nonseitu   | ricarica telefonica | Conferma Ricarica telefonica effettuata |
  
  @BPINPROAND-1550
  Scenario Outline: Ricarica Telefonica da carta
      Viene effettuata una ricarica telefonica da carta

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1550 | <onboarding> |
    And Viene effettuata una "ricarica telefonica" da "conti e carte" section
      | payWith   | amount   | testIdCheck | posteid   | phoneNumber   | phoneCompaign   | paymentMethod   |
      | <payWith> | <amount> | -           | <posteID> | <phoneNumber> | <phoneCompaign> | <paymentMethod> |
    And Salvare l'operazione come "operazione veloce"
      | quickOperationName   |
      | <quickOperationName> |
    And Viene controllata la ricezione della notifica
      | messageText   |
      | <messageText> |
    And Viene effettuata la transazione da operazione veloce
      | quickOperationName   | testIdCheck |
      | <quickOperationName> | -           |
    And Viene controllata la ricezione della notifica
      | messageText   | testIdCheck |
      | <messageText> | -           |
    And Viene controllata la corretta categorizzazione della transazione
      | categoryTransation   | amount   | testIdCheck |
      | <categoryTransation> | <amount> | -           |
    Then Viene controllata la ricezione della notifica
      | messageText   | notifyType   |
      | <messageText> | <notifyType> |
    And Viene chiusa l'app

    @BPINPROAND-1550_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith                   | phoneNumber | phoneCompaign | amount | messageText         | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description         | paymentMethod | onboarding | notifyType          | messageText                             |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | postepay;5333171077147177 |   356874982 | PosteMobile   |  15,00 | ricarica telefonica | quickOperationName | editValue | categoryTransation | month | categoryDestination | prisma  | ricarica telefonica | carta         | nonseitu   | ricarica telefonica | Conferma Ricarica telefonica effettuata |

  #Per questo test c'è bisogno di un numero PosteMobile
  #   Examples:
  #      | username           | password     | loginType | userHeader | device   | payWith                   | phoneNumber   | phoneCompaign | amount | messageText     | quickOperationName | editValue | categoryTransation | month | categoryDestination | posteID | description |
  #      | musellasal@gmail.com | Musabp67!| poste.it  | Salvatore       | emulator | postepay;5333171077147177 | 3804678064    | WIND 		   |  15,00 | test automatico | quickOperationName | editValue | categoryTransation | month | categoryDestination | prisma  | Test        |
  #
  @BPINPROAND-1555
  Scenario Outline: Ricarica Telefonica Operazione veloce
      Viene effettuata una ricarica telefonica da operazione veloce

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1555 | <onboarding> |
    And Viene effettuata la transazione da operazione veloce
      | quickOperationName   | transactionType   | testIdCheck | payWith   | transferTo   | owner    | amount   | description   |
      | <quickOperationName> | <transactionType> | -           | <payWith> | <transferTo> | <owner > | <amount> | <description> |
    #    And Viene effettuata una ricarica telefonica da operazione veloce
    And L utente effettua una ricarica telefonica da operazione veloce
      | payWith   | amount   | posteid   | testIdCheck | phoneNumber   | phoneCompaign   |
      | <payWith> | <amount> | <posteID> | -           | <phoneNumber> | <phoneCompaign> |
    And Viene controllato che la transazione sia stata registrata nella lista movimenti
      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    #    And Controlla che il saldo sia stato decrementato da "conto bancoposta"
    #      | conto     | amount   | operation | transactionType   | transactionDescription   | commissioni   |
    #      | <payWith> | <amount> | -         | <transactionType> | <transactionDescription> | <commissioni> |
    Then Viene controllata la ricezione della notifica
      | messageText   | notifyType   |
      | <messageText> | <notifyType> |
    And Viene chiusa l'app

    @BPINPROAND-1555_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText                                     | quickOperationName  | editValue | categoryTransation | month | categoryDestination | posteID | description         | transactionType | notifyType          | onboarding | messageText                             |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 |   356874982 | PosteMobile   |  15,00 | Mobile: Conferma ricarica Telefonica con Mobile | Ricarica telefonica | editValue | categoryTransation | month | categoryDestination | prisma  | Ricarica telefonica | telefonica      | ricarica telefonica | nonseitu   | Conferma Ricarica telefonica effettuata |

    @BPINPROAND-1555_vol
    Examples: 
      | username  | password   | loginType | userHeader | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText                                     | quickOperationName  | editValue | categoryTransation | month | categoryDestination | posteID | description         | transactionType | notifyType          | onboarding | messageText                             |
      | fedro.vol | Password1! | poste.it  | Fedro      | samsungGalaxyS8_plus | conto;001009125285 |   356874982 | PosteMobile   |   5,00 | Mobile: Conferma ricarica Telefonica con Mobile | Ricarica telefonica | editValue | categoryTransation | month | categoryDestination |  123123 | Ricarica telefonica | telefonica      | ricarica telefonica | nonseitu   | Conferma Ricarica telefonica effettuata |

    @BPINPROAND-1555_romani
    Examples: 
      | username          | password   | loginType | userHeader   | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText                                     | quickOperationName  | editValue | categoryTransation | month | categoryDestination | posteID | description         | transactionType | notifyType          | onboarding | messageText                             |
      | prisma20@poste.it | Password2! | poste.it  | Maria Grazia | samsungGalaxyS8_plus | conto;001009187830 |   356874982 | PosteMobile   |  5,00 | Mobile: Conferma ricarica Telefonica con Mobile | Ricarica telefonica | editValue | categoryTransation | month | categoryDestination |  123123 | Ricarica telefonica | telefonica      | ricarica telefonica | nonseitu   | Conferma Ricarica telefonica effettuata |

    @BPINPROAND-1555_Agata
    Examples: 
      | username          | password   | loginType | userHeader | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText                                     | quickOperationName  | editValue | categoryTransation | month | categoryDestination | posteID | description         | transactionType | notifyType          | onboarding | messageText                             |
      | prisma66@poste.it | Password1! | poste.it  | Agata      | samsungGalaxyS8_plus | conto;001009200278 |   356874982 | PosteMobile   |  5,00 | Mobile: Conferma ricarica Telefonica con Mobile | Ricarica telefonica | editValue | categoryTransation | month | categoryDestination |  123123 | Ricarica telefonica | telefonica      | ricarica telefonica | nonseitu   | Conferma Ricarica telefonica effettuata |

    @BPINPROAND-1555_fiorella
    Examples: 
      | username          | password   | loginType | userHeader | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText                                     | quickOperationName  | editValue | categoryTransation | month | categoryDestination | posteID | description         | transactionType | notifyType          | onboarding | messageText                             |
      | prisma65@poste.it | Password1! | poste.it  | Fiorella      | samsungGalaxyS8_plus | conto;001009197300 |   356874982 | PosteMobile   |  5,00 | Mobile: Conferma ricarica Telefonica con Mobile | Ricarica telefonica | editValue | categoryTransation | month | categoryDestination |  123123 | Ricarica telefonica | telefonica      | ricarica telefonica | nonseitu   | Conferma Ricarica telefonica effettuata |

  @BPINPROAND-4008
  Scenario Outline: Ricarica Telefonica
    Ricarica Telefonica da carta,inserimento dati manuale

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck     | onboarding   |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | BPINPROAND-1550 | <onboarding> |
    And Viene effettuata una "ricarica telefonica" da "conti e carte" section
      | payWith   | amount   | testIdCheck | posteid | phoneNumber   | phoneCompaign   |
      | <payWith> | <amount> | -           |  123123 | <phoneNumber> | <phoneCompaign> |
    And Viene chiusa l'app

    @BPINPROAND-4008_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText                                     | quickOperationName  | editValue | categoryTransation | month | categoryDestination | posteID | description         | transactionType | notifyType          | onboarding | messageText                             |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | conto;001044548509 |   356874982 | PosteMobile   |  15,00 | Mobile: Conferma ricarica Telefonica con Mobile | ricarica telefonica | editValue | categoryTransation | month | categoryDestination | prisma  | Ricarica telefonica | telefonica      | ricarica telefonica | nonseitu   | Conferma Ricarica telefonica effettuata |

    @BPINPROAND-4008_fortunato
    Examples: 
      | username                | password        | loginType | userHeader | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText                                     | quickOperationName  | editValue | categoryTransation | month | categoryDestination | posteID | description         | transactionType | notifyType          | onboarding | messageText                             |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | poste.it  | Vincenzo   | samsungGalaxyS8_plus | conto;001044920229 |   356874982 | PosteMobile   |  15,00 | Mobile: Conferma ricarica Telefonica con Mobile | ricarica telefonica | editValue | categoryTransation | month | categoryDestination | prisma  | Ricarica telefonica | telefonica      | ricarica telefonica | nonseitu   | Conferma Ricarica telefonica effettuata |

    @BPINPROAND-4008_vol
    Examples: 
      | username  | password   | loginType | userHeader | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText                                     | quickOperationName  | editValue | categoryTransation | month | categoryDestination | posteID | description         | transactionType | notifyType          | onboarding | messageText                             |
      | fedro.vol | Password1! | poste.it  | Fedro      | samsungGalaxyS8_plus | conto;001009125285 |   356874982 | PosteMobile   |  15,00 | Mobile: Conferma ricarica Telefonica con Mobile | ricarica telefonica | editValue | categoryTransation | month | categoryDestination |  123123 | Ricarica telefonica | telefonica      | ricarica telefonica | nonseitu   | Conferma Ricarica telefonica effettuata |

    @BPINPROAND-4008_romani
    Examples: 
      | username          | password   | loginType | userHeader   | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText                                     | quickOperationName  | editValue | categoryTransation | month | categoryDestination | posteID | description         | transactionType | notifyType          | onboarding | messageText                             |
      | prisma20@poste.it | Password2! | poste.it  | Maria Grazia | samsungGalaxyS8_plus | conto;001009187830 |   356874982 | PosteMobile   |  15,00 | Mobile: Conferma ricarica Telefonica con Mobile | ricarica telefonica | editValue | categoryTransation | month | categoryDestination |  123123 | Ricarica telefonica | telefonica      | ricarica telefonica | nonseitu   | Conferma Ricarica telefonica effettuata |

    @BPINPROAND-4008_fiorella
    Examples: 
      | username          | password   | loginType | userHeader | device               | payWith            | phoneNumber | phoneCompaign | amount | messageText                                     | quickOperationName  | editValue | categoryTransation | month | categoryDestination | posteID | description         | transactionType | notifyType          | onboarding | messageText                             |
      | prisma65@poste.it | Password1! | poste.it  | Fiorella   | samsungGalaxyS8_plus | conto;001009197300 |   356874982 | PosteMobile   |  15,00 | Mobile: Conferma ricarica Telefonica con Mobile | ricarica telefonica | editValue | categoryTransation | month | categoryDestination |  123123 | Ricarica telefonica | telefonica      | ricarica telefonica | nonseitu   | Conferma Ricarica telefonica effettuata |
      