@Bacheca
Feature: Bacheca
  L utente accede all'area bacheca

  @BPINPROAND-1572 @BPINPROIOS-1422
  Scenario Outline: Verificare l accesso alla Bacheca
      Verificare l accesso alla Bacheca

    Given L'app "android" viene avviata e viene eseguito il New Login
      | username   | password   | loginType   | userHeader   | device   | posteid   | testIdCheck   | onboarding   | debug |
      | <username> | <password> | <loginType> | <userHeader> | <device> | <posteID> | <testIdCheck> | <onboarding> | y     |
    And Naviga verso la sezione Bacheca
    And Viene chiusa l'app

    @BPINPROAND-1572_musella
    Examples: 
      | username             | password  | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto |
      | musellasal@gmail.com | Musabp67! | poste.it  | Salvatore  | samsungGalaxyS8_plus | prisma  | BPINPROAND-1572 | nonseitu   | Libretto |

    @BPINPROAND-1572_vol
    Examples: 
      | username  | password   | loginType | userHeader | device               | posteID | testIdCheck     | onboarding | biscotto |
      | fedro.vol | Password1! | poste.it  | Fedro      | samsungGalaxyS8_plus |  123123 | BPINPROAND-1572 | nonseitu   | Libretto |

    @BPINPROAND-1572_romani
    Examples: 
      | username          | password   | loginType | userHeader   | device               | posteID | testIdCheck     | onboarding | biscotto |
      | prisma20@poste.it | Password2! | poste.it  | Maria Grazia | samsungGalaxyS8_plus |  123123 | BPINPROAND-1572 | nonseitu   | Libretto |
      
      @BPINPROAND-1572_fiorella
    Examples: 
      | username          | password   | loginType | userHeader   | device               | posteID | testIdCheck     | onboarding | biscotto |
      | prisma65@poste.it | Password1! | poste.it  | Fiorella     | samsungGalaxyS8_plus |  123123 | BPINPROAND-1572 | nonseitu   | Libretto |
      
      @BPINPROAND-1572_agata
    Examples: 
      | username          | password   | loginType | userHeader   | device               | posteID | testIdCheck     | onboarding | biscotto |
      | prisma66@poste.it | Password1! | poste.it  | Agata | samsungGalaxyS8_plus |  123123 | BPINPROAND-1572 | nonseitu   | Libretto |

     @BPINPROIOS-1422_mancini
      Examples: 
      | username          | password   | loginType | userHeader   | device               | posteID | testIdCheck     | onboarding |
      | prisma21@poste.it | Password1! | poste.it  | Antonio      | samsungGalaxyS8_plus |  123123 | BPINPROIOS-1422 | nonseitu   | 
      
     @BPINPROIOS-1422_fanucci
      Examples: 
      | username          | password   | loginType | userHeader    | device               | posteID | testIdCheck     | onboarding |
      | prisma33@poste.it | Password1! | poste.it  | Vincenza      | samsungGalaxyS8_plus |  123123 | BPINPROIOS-1422 | nonseitu   | 
      
      @BPINPROAND-1422_fiorella
      Examples: 
      | username          | password   | loginType | userHeader    | device               | posteID | testIdCheck     | onboarding |
      | prisma65@poste.it | Password1! | poste.it  | Fiorella      | samsungGalaxyS8_plus |  123123 | BPINPROIOS-1422 | nonseitu   | 
      
      